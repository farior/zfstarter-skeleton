<?php

use ZFCTool\Service\Migration\AbstractMigration;

class Migration_20150302_110645_20 extends AbstractMigration
{
    /**
     * Upgrade
     */
    public function up()
    {

        $this->query("DROP TABLE IF EXISTS `zfs_roles`");
        $this->query("CREATE TABLE `zfs_roles` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(255) NOT NULL,
          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
        $this->query("DROP TABLE IF EXISTS `zfs_privileges`");
        $this->query("CREATE TABLE `zfs_privileges` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `role_id` int(10) unsigned NOT NULL,
          `privilege` varchar(64) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `role_id` (`role_id`),
          CONSTRAINT `zfs_privileges_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `zfs_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
        $this->query("DROP TABLE IF EXISTS `zfs_roles_inheritance`");
        $this->query("CREATE TABLE `zfs_roles_inheritance` (
          `parent_role` int(10) unsigned NOT NULL,
          `child_role` int(10) unsigned NOT NULL,
          PRIMARY KEY (`parent_role`,`child_role`),
          KEY `child_role` (`child_role`),
          CONSTRAINT `zfs_roles_inheritance_ibfk_1` FOREIGN KEY (`parent_role`) REFERENCES `zfs_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT `zfs_roles_inheritance_ibfk_2` FOREIGN KEY (`child_role`) REFERENCES `zfs_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->query("INSERT INTO `zfs_roles` (`id`, `name`, `created`) VALUES ('1', 'guest', '2015-01-27 12:16:56')");
        $this->query("INSERT INTO `zfs_roles` (`id`, `name`, `created`) VALUES ('2', 'user', '2015-01-27 12:16:56')");
        $this->query("INSERT INTO `zfs_roles` (`id`, `name`, `created`) VALUES ('3', 'admin', '2015-01-27 12:16:56')");

        $this->query("INSERT INTO `zfs_roles_inheritance` (`parent_role`, `child_role`) VALUES ('3', '2')");
    }

    /**
     * Degrade
     */
    public function down()
    {
        $this->query("DROP TABLE IF EXISTS `zfs_roles_inheritance`");
        $this->query("DROP TABLE IF EXISTS `zfs_privileges`");
        $this->query("DROP TABLE IF EXISTS `zfs_roles`");
    }
}
