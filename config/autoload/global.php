<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db' => array(
        'driver'   => 'Pdo',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'mail' => array(
        'transport' => array(
            'host' => 'smtp.gmail.com',
            'name' => 'smtp.gmail.com',
            'port' => 465,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'some@gmail.com',
                'password' => 'some_password',
                'ssl' => 'ssl',
            ),
        ),
        'defaultFrom' => array(
            'email' => 'zfstarter@nixsolutions.com',
            'name' => 'Star Sender'
        ),
        'headers' => array(
            'PROJECT' => 'zfstarter',
        ),
    ),
    'opauth' => array(
        'security_salt' => 'f2a0177fadfdb1a7c841c672144693587e224857d230e7f53db1271b49c0b925',
        'Strategy' => array(
            'Facebook' => array(
                'app_id'     => 'facebook_app_id',
                'app_secret' => 'facebook_app_secret',
                'scope'      => 'email'
            ),
            'Twitter' => array(
                'key'    => 'twitter_key',
                'secret' => 'twitter_secret'
            ),
            'Google' => array(
                'client_id'     => 'google_client_id',
                'client_secret' => 'google_client_secret'
            )
        ),
        'callback_transport' => 'post'
    ),
    'ZFCTool' => array(
        'migrations' => array(
            'modulesDirectoryPath' => array(
                'module',
                'vendor/zfstarter'
            ),
        ),
    )
);
