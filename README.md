ZFStarter Skeleton Application
=======================

Installation
------------

Step 1: Clone or download Skeleton from Bitbucket

    cd my/project/dir
    git clone https://farior@bitbucket.org/farior/zfstarter-skeleton.git
    cd zfstarter-skeleton

Step 2: Install composer and dependencies

    curl -sS https://getcomposer.org/installer | php
    php composer.phar install

Step 3: Set up db config

    copy /config/autoload/local.php.dist to /config/autoload/local.php
    open and change your db settings

Step 4: Apply migrations

    php vendor/bin/zfc.php up db -i